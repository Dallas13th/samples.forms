﻿using Android.Content;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Samples.Forms.CustomMaps;
using Samples.Forms.Droid.CustomMaps;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.Android;
using View = Android.Views.View;

[assembly: ExportRenderer(typeof(CustomMap), typeof(CustomMapRenderer))]
namespace Samples.Forms.Droid.CustomMaps
{
    public class CustomMapRenderer : MapRenderer, GoogleMap.IInfoWindowAdapter
    {
        IList<Pin> _formsPins;

        public CustomMapRenderer(Context context)
            : base(context)
        {
            _formsPins = new List<Pin>();
        }

        protected override void OnElementChanged(Xamarin.Forms.Platform.Android.ElementChangedEventArgs<Map> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                var formsMap = (CustomMap)e.NewElement;
                _formsPins = formsMap.Pins;
            }
        }
        
        protected override void OnMapReady(GoogleMap map)
        {
            base.OnMapReady(map);

            NativeMap.SetInfoWindowAdapter(this);
        }

        protected override MarkerOptions CreateMarker(Pin pin)
        {
            var marker = new MarkerOptions();
            marker.SetPosition(new LatLng(pin.Position.Latitude, pin.Position.Longitude));
            marker.SetTitle(pin.Label);
            marker.SetSnippet(pin.Address);
            
            return marker;
        }

        public View GetInfoContents(Marker marker)
        {
            
            var inflater = Android.App.Application.Context.GetSystemService(Context.LayoutInflaterService) as Android.Views.LayoutInflater;
            if (inflater == null) return null;

            var customPin = GetCustomPin(marker);

            if (customPin == null)
            {
                throw new Exception("Custom pin not found");
            }

            var view = inflater.Inflate(Resource.Layout.MapInfoWindow, null);

            var infoTitle = view.FindViewById<TextView>(Resource.Id.mapInfoWindowTitle);
            var infoSubtitle = view.FindViewById<TextView>(Resource.Id.mapInfoWindowSubtitle);

            if (infoTitle != null)
            {
                infoTitle.Text = marker.Title;
            }

            if (infoSubtitle != null)
            {
                infoSubtitle.Text = marker.Snippet;
            }

            return view;
        }

        public View GetInfoWindow(Marker marker)
        {
            return null;
        }

        private Pin GetCustomPin(Marker annotation)
        {
            var position = new Position(annotation.Position.Latitude, annotation.Position.Longitude);

            return _formsPins.FirstOrDefault(w => w.Position == position);
        }
    }
}