﻿namespace Samples.Forms.Constants
{
    public static class AlertConstants
    {
        public const string CreateNewLandmark = "Create new landmark";
        public const string MoveSelectedOrPrevious = "Move selected or previous";
    }
}