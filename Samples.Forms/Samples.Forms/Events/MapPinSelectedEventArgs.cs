﻿using System;
using Xamarin.Forms.Maps;

namespace Samples.Forms.Events
{
    public class MapPinSelectedEventArgs : EventArgs
    {
        public MapPinSelectedEventArgs(Pin pin)
        {
            Pin = pin;
        }

        public Pin Pin { get; }
    }
}