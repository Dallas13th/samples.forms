﻿using System.Collections.Generic;
using Samples.Forms.Constants;
using Samples.Forms.Events;
using Samples.Forms.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Samples.Forms
{
    public partial class MainPage : ContentPage
    {
        private Pin _selectedPin;
        private MainPageViewModel _viewModel;

        public MainPage()
        {
            InitializeComponent();
            _viewModel = new MainPageViewModel();
            _viewModel.OnMapPinSelected += ViewModelOnOnMapPinSelected;
            BindingContext = _viewModel;
        }

        private async void Map_OnMapClicked(object sender, MapClickedEventArgs e)
        {
            if (_selectedPin == null)
            {
                CreateLandmark(e.Position);
                return;
            }

            var action = await DisplayActionSheet($"Coordinates: lat={e.Position.Latitude:0.000000}, lng={e.Position.Longitude:0.000000}", 
                "Cancel",
                null,
                AlertConstants.CreateNewLandmark, AlertConstants.MoveSelectedOrPrevious);
            switch (action)
            {
                case AlertConstants.CreateNewLandmark:
                    CreateLandmark(e.Position);
                    break;
                case AlertConstants.MoveSelectedOrPrevious:
                    MoveSelectedLandMark(e.Position);
                    break;
            }
        }

        private async void PinOnInfoWindowClicked(object sender, PinClickedEventArgs e)
        {
            var pin = (Pin) sender;

            var isConfirmed = await DisplayAlert("Warning", "Are you sure want to delete?", "Yes", "Cancel");

            if (!isConfirmed) return;

            DeleteLandmark(pin);
        }

        private void PinOnMarkerClicked(object sender, PinClickedEventArgs e)
        {
            _selectedPin = (Pin) sender;
        }

        private void ViewModelOnOnMapPinSelected(object sender, MapPinSelectedEventArgs e)
        {
            var mapSpan = MapSpan.FromCenterAndRadius(e.Pin.Position, Distance.FromKilometers(4));
            map.MoveToRegion(mapSpan);
            _selectedPin = e.Pin;
        }

        private void CreateLandmark(Position pos)
        {
            var pin = new Pin
            {
                Label = "My Landmark",
                Address = $"lat={pos.Latitude:0.000000}, lng={pos.Longitude:0.000000}",
                Type = PinType.Place,
                Position = pos
            };

            pin.MarkerClicked += PinOnMarkerClicked;
            pin.InfoWindowClicked += PinOnInfoWindowClicked;

            map.Pins.Add(pin);
            _viewModel.MapPins.Add(pin);
        }

        private async void MoveSelectedLandMark(Position newPosition)
        {
            if (_selectedPin == null)
            {
                await DisplayAlert("Error", "Select Map pin first", "OK");
                return;
            }

            _selectedPin.Position = newPosition;
        }

        private void DeleteLandmark(Pin pin)
        {
            map.Pins.Remove(pin);
            _viewModel.MapPins.Remove(pin);
            if (_selectedPin == pin)
            {
                _selectedPin = null;
            }
        }
    }
}
