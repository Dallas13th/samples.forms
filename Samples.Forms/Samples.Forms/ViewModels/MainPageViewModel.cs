﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Samples.Forms.Annotations;
using Samples.Forms.Events;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Samples.Forms.ViewModels
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<Pin> MapPins { get; }
        
        public MainPageViewModel()
        {
            MapPins = new ObservableCollection<Pin>();
            OnMapPinTappedCommand = new Command<Pin>(OnMapPinTapped);
        }

        public event EventHandler<MapPinSelectedEventArgs> OnMapPinSelected; 

        private void OnMapPinTapped(Pin pin)
        {
            OnMapPinSelected?.Invoke(this, new MapPinSelectedEventArgs(pin));
        }

        public Command<Pin> OnMapPinTappedCommand { get; }
        
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
